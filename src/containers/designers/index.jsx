import React, { Fragment, PureComponent } from 'react'
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { createPropsSelector } from "reselect-immutable-helpers"

import packageJson from '../../../package.json'
// selectors
import {getDesignersList, getIsDesignersLoaded} from "./selectors"

// actions
import {fetchDesignersData} from './actions'
import {setDesignerdata} from '../designer/actions'

// Styles
import {
    DesignersList,
    DesignersListItem,
    MainContainer,
    LoadingSkeleton
} from '../../components/styles/styles'

const designerItemSkeleton = () => {
    const arr = []
    let counter = 0
    while(counter < 5) {
        arr.push(
            <div key={counter} style={{marginBottom: '20px'}}>
                <LoadingSkeleton />
            </div>
        )

        counter++
    }

    return arr
}

class Designers  extends PureComponent {
    constructor(props) {
        super(props)

        this.state = {
            error: null,
            designer: null
          }

       this.handleDisgnerClick = this.handleDisgnerClick.bind(this)
    }

    handleDisgnerClick(e, designer) {
        e.preventDefault()
        // pass data to designer store
        this.props.setDesignerdata(designer)
        this.props.history.push(`/designers/${designer.id}`)
    }

    componentDidMount() {
        // ONLY make network request once - on initial page load
        if(!this.props.isDesignersLoaded) {
            this.props.fetchDesignersData(packageJson.endpointUrl)
            .catch(({message}) => {
                this.setState({
                    error: message
                })
            })
        }
    }

    render() {
        const {isDesignersLoaded, designers } = this.props

        if(this.state.error) {
            throw new Error(this.state.error)
        }

        return (
            <MainContainer>
                {
                    !isDesignersLoaded ? designerItemSkeleton()
                    :
                    <Fragment>
                        <h2>Our Designers</h2>
                        <DesignersList>
                            {designers.map(designer => (
                                <DesignersListItem key={designer.id} onClick={(e) => this.handleDisgnerClick(e, designer)}>
                                    <h4>{designer.name} </h4>
                                    <section>
                                        {`${designer.about.text.substring(1, 50)}[...]`}
                                    </section>
                                </DesignersListItem>
                            ))}
                        </DesignersList>
                    </Fragment>
                }
            </MainContainer>
        )
    }
}

Designers.propTypes = {
    designers: PropTypes.arrayOf(PropTypes.object),
    isDesignersLoaded: PropTypes.bool
}

const mapStateToProps = createPropsSelector({
    designers: getDesignersList,
    isDesignersLoaded: getIsDesignersLoaded
})

const mapDispatchToProps = {
    fetchDesignersData,
    setDesignerdata
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Designers)