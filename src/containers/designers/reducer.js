import { handleActions } from "redux-actions"
import Immutable from "immutable"
import { mergePayload } from "../../utils/utils"
import { 
	receiveDesignersData,
} from "../../app/create-actions"

export default handleActions({
		[receiveDesignersData]: mergePayload
	},
	Immutable.Map()
)