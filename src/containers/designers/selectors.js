import Immutable from "immutable"
import { createSelector } from "reselect"
import {createGetSelector} from "reselect-immutable-helpers"
import { getUi } from "../../utils/selector-utils"

export const getDesigners = createSelector(getUi, ({ designers }) => designers)
export const getDesignersList = createGetSelector(
    getDesigners,
    'designersList',
    Immutable.List()
)

export const getIsDesignersLoaded = createGetSelector(
    getDesigners,
    'isDesignersLoaded',
    false
)
