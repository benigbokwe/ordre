// connectors
import { 
	initDesignersData,
} from "../../connectors/commands"

import {HTTP_HEADERS} from './constants'

export const fetchDesignersData = (url) => (dispatch) => {
    const options = {
        headers: HTTP_HEADERS
    }
   
	return dispatch(initDesignersData(url, options))
}
