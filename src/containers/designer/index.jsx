import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import { connect } from "react-redux"
import { createPropsSelector } from "reselect-immutable-helpers"

import DangerousHTML from '../../components/dangerous-html'

import packageJson from '../../../package.json'

// actions
import {fetchDesignerData} from './actions'

// selectors
import {getDesignerData, getIsDesignerLoaded} from './selectors'

// Styles 
import {
    MainContainer, 
    DesignerContainer,
    LoadingSkeleton
} from '../../components/styles/styles'

const designerItemSkeleton = () => {
    const arr = []
    let counter = 0
    while(counter < 5) {
        arr.push(
            <div key={counter} style={{marginBottom: '20px'}}>
                <LoadingSkeleton />
            </div>
        )

        counter++
    }

    return arr
}
class Designer  extends PureComponent {

    constructor(prop) {
        super(prop)

        this.state = {
            error: null
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id

        if (!this.props.isDesignerLoaded) {
            this.props.fetchDesignerData(`${packageJson.endpointUrl}${id}`)
            .catch(({message}) => {
                this.setState({
                    error: message
                })
            })
        }
    }

    render() {
        const { isDesignerLoaded, designer} = this.props

        if(this.state.error) {
            throw new Error(this.state.error)
        }

        return (
            <MainContainer>
               {
                   !isDesignerLoaded 
                        ? designerItemSkeleton()
                        : 
                        <DesignerContainer>
                            <h3>{designer.name}</h3>
                            <article>
                                <DangerousHTML htmlObj={designer.about.source} />
                            </article>
                        </DesignerContainer>
               } 
            </MainContainer>
        )
      }
}

Designer.propTypes = {
    designer: PropTypes.object,
    isDesignerLoaded: PropTypes.bool
}

const mapStateToProps = createPropsSelector({
    designer: getDesignerData,
    isDesignerLoaded: getIsDesignerLoaded
})

const mapDispatchToProps = {
	fetchDesignerData
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Designer)