import Immutable from "immutable"
import { createSelector } from "reselect"
import {createGetSelector} from "reselect-immutable-helpers"
import { getUi } from "../../utils/selector-utils"

export const getDesigner = createSelector(getUi, ({ designer }) => designer)
export const getDesignerData = createGetSelector(
    getDesigner,
    'designerData',
    Immutable.Map()
)

export const getIsDesignerLoaded = createGetSelector(
    getDesigner,
    'isDesignerLoaded',
    false
)