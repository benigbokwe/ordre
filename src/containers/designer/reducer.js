import { handleActions } from "redux-actions"
import Immutable from "immutable"
import { mergePayload } from "../../utils/utils"
import { 
	receiveDesignerData,
} from "../../app/create-actions"

export default handleActions({
		[receiveDesignerData]: mergePayload
	},
	Immutable.Map()
)