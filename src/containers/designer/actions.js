
// connectors
import { 
	initDesignerData,
} from "../../connectors/commands"

import {
	receiveDesignerData
} from "../../app/create-actions"

import {HTTP_HEADERS} from '../designers/constants'

export const fetchDesignerData = (url) => (dispatch) => {
    const options = {
        headers: HTTP_HEADERS
    }

	return dispatch(initDesignerData(url, options))
}

export const setDesignerdata  = (designerData) => (dispatch) => {
    return dispatch(receiveDesignerData({
        designerData,
        isDesignerLoaded: true
    }))
}
