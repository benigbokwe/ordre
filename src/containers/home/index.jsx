import React, { Fragment, PureComponent } from 'react'
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { createPropsSelector } from "reselect-immutable-helpers"
import { Link } from 'react-router-dom'

import packageJson from '../../../package.json'
// selectors
import {getDesignersList, getIsDesignersLoaded} from "../designers/selectors"

// actions
import {fetchDesignersData} from '../designers/actions'

// Styles
import {
    MainContainer,
    LoadingSkeleton
} from '../../components/styles/styles'

class Home  extends PureComponent {
    constructor(props) {
        super(props)

        this.state = {
            error: null
          }
    }

    componentDidMount() {
        // ONLY make network request once - on initial page load
        if(!this.props.isDesignersLoaded) {
            this.props.fetchDesignersData(packageJson.endpointUrl)
            .catch(({message}) => {
                this.setState({
                    error: message
                })
            })
        }
    }

    render() {
        const {isDesignersLoaded, designers } = this.props

        if(this.state.error) {
            throw new Error(this.state.error)
        }

        return (
            <MainContainer>
                {
                    !isDesignersLoaded ? <LoadingSkeleton />
                    :
                    <Fragment>
                        <h2>Welcome to project home page</h2>
                        <h3>We currently have {designers.length} desginers!</h3>
                        <p>
                            Click <Link to={'/designers'}>here</Link> to view all
                        </p>
                </Fragment>
                }
            </MainContainer>
        )
    }
}

Home.propTypes = {
    designers: PropTypes.arrayOf(PropTypes.object),
    isDesignersLoaded: PropTypes.bool
}

const mapStateToProps = createPropsSelector({
    designers: getDesignersList,
    isDesignersLoaded: getIsDesignersLoaded
})

const mapDispatchToProps = {
    fetchDesignersData
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Home)