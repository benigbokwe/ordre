import styled,  {keyframes} from 'styled-components'


export const Wrapper = styled.div`
    padding: 0;
    margin: auto;
    background: #fff;
    width: 100%;
    padding: 5px;
`;

export const MainContainer = styled.div`
    padding: 30px 50px;
`;

export const HeaderNav = styled.ul`
    list-style: none;
    margin: 0;
    padding: 0 0 0 20px;
    overflow: hidden;
    background-color: #333;
    display: flex;
`;

export const HeaderNavList = styled.li`
    margin: auto;

    a {
      color: #fff;
      margin: 0 10px;
      text-decoration: none;
    }
`;

export const FooterWrap = styled.footer`
    margin: 0;
    padding: 20px;
    background-color: #333;
    color: #fff;

    section {
      width: calc(100% / 4);
      margin: auto;
    }
`;

export const ErrorSection = styled.section`
    color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
    padding: 15px;
    margin: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
`;

/**
 * Designers & Designer styling
 */
export const DesignersList = styled.div`
    margin: auto;
    display: flex;
    text-align: center;
    flex-flow: row wrap;
    justify-content: space-around;

    &::after {
      content: "";
      clear: both;
      display: table;
    }
`;

export const DesignersListItem = styled.div`
    margin: 20px;
    width: calc(100% * 0.2222);
    padding: 10px;
    border: 1px solid #ccc;
    flex: 1 auto;

    &:hover {
      cursor: pointer;
      opacity: 0.5;
      border: 1px solid #FF6347;
    }

    h4 {
      overflow-wrap: break-word;
      font-weight: 400;
      color: #008080;
    }
`;

export const DesignerContainer = styled.section`

`;
/**
 * end of designers styling================
 */
const spin = keyframes`
    0%{transform: rotate(0deg)}
    100%{ transform: rotate(360deg)}
`;

const placeHolderShimmer = keyframes`
    0%{background-position: -468px 0}
    100%{background-position: 468px 0}
`;

/**
 * Loading animation
 */
export const LoadingAnimation = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    background-color: rgba(0,0,0,.7);
    color: white;
    height: 100%;
    width: 100%;

    section {
      border: 16px solid #06d3d3;
      border-top: 16px solid #008080
      border-radius: 50%;
      width: 120px;
      height: 120px;
      animation: ${spin} 2s linear infinite;
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      margin: auto;
    } 
`;

export const LoadingSkeleton = styled.div`
    animation-fill-mode: forwards;
    animation: ${placeHolderShimmer} 1s linear infinite;
    background: #f6f7f8;
    background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
    background-size: 800px 100px;
    height: 100px;
    position: relative;
`;
 /**
  * END loading animation
  */

export const Button = styled.button`
    background: ${props => props.primary ? "palevioletred" : "white"};
    color: ${props => props.primary ? "white" : "palevioletred"};

    font-size: 1em;
    margin: 1em;
    padding: 0.25em 1em;
    border: 2px solid palevioletred;
    border-radius: 3px;
`;