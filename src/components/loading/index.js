import React from 'react'
import {LoadingAnimation} from '../styles/styles'

const Loading = (props) => {
    return (
        <LoadingAnimation><section>&nbsp;</section></LoadingAnimation>
    )
}

export default Loading