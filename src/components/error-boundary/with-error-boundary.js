import React from 'react'
import ErrorFallBack from './partials/error-fallback'

/**
 * Not in use now as it's advised not to use 
 * HOC for error boundaries - They are not necessary
 * @param {Component} Component 
 */
export const withErrorBoundary = (Component) => {
    class WithErrorBoundary extends React.Component {
        constructor (props) {
            super(props)
        
            // Construct the initial state
            this.state = {
            hasError: false,
            error: null,
            errorInfo: null
            }
        }
  
        componentDidCatch (error, info) {
            // Update state if error happens
            this.setState({ hasError: true, error, errorInfo: info })
        }
  
      render () {
        // if state contains error we render fallback component
        if (this.state.hasError) {
          const { error, errorInfo } = this.state
          return (
            <ErrorFallBack
              {...this.props}
              error={error}
              errorInfo={errorInfo}
            />
          )
        }
  
        return <Component {...this.props} />
      }
    }

    WithErrorBoundary.displayName = `WithErrorBoundary(${Component.displayName})`

    return WithErrorBoundary
  }
