import React from 'react'

const ErrorFallBack  = ({error, errorInfo}) => (
    <div className="error-boundary-fallback">
        <summary>{error}</summary>
        {/* <details style={{ whiteSpace: 'pre-wrap' }}>
            { error && error.toString() }
            { errorInfo.componentStack }
        </details> */}
    </div>
)

export default ErrorFallBack