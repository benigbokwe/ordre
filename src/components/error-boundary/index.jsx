import React, { Component } from 'react'

import {ErrorSection} from '../../components/styles/styles'

/**
 * Error boundaries are primarily useful for production, 
 * but in development we want to make errors as highly visible as possible.
 */
class ErrorBoundary extends Component {
    constructor(props){
        super(props)
        this.state = { error: null, errorInfo: null }
    }

    componentDidCatch(error, info){
        this.setState({ error: error, errorInfo: info })
    }

    render() {
        if(this.state.errorInfo){
            return (
                <ErrorSection>
                    <summary>{this.state.error.toString()}</summary>
                    {/* <details style={{ whiteSpace: 'pre-wrap' }}>
                        { this.state.error && this.state.error.toString() }
                        { this.state.errorInfo.componentStack }
                    </details> */}
                </ErrorSection>
            )
      }

      return this.props.children
    }
}

export default ErrorBoundary


