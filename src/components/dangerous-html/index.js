import React from 'react'

const createMarkup = (htmlObj) => ({
    __html: htmlObj
})

const DangerousHTML = ({htmlObj}) => {
    return (
        <div dangerouslySetInnerHTML={createMarkup(htmlObj)} />
    )
}

export default DangerousHTML