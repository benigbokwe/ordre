import React from 'react'
import { Link } from 'react-router-dom'
import {HeaderNav, HeaderNavList} from '../styles/styles'

const Header = () => {
    return (
        <HeaderNav>
            <HeaderNav>
                <HeaderNavList><h1><Link to="/">Project</Link></h1></HeaderNavList>
                <HeaderNavList><Link to="/designers">Designers</Link></HeaderNavList>
            </HeaderNav>
        </HeaderNav>
    )
}

export default Header