import React from 'react'
import {FooterWrap} from '../styles/styles'

const Footer = () => {
    return (
        <FooterWrap>
            <section>&copy; {(new Date()).getFullYear()} - Ben Igbokwe</section>
        </FooterWrap>
    )
}

export default Footer