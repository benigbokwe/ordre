import { combineReducers } from "redux"

// import reducers
import appReducer from '../app/reducer'
import designerReducer from "../containers/designer/reducer"
import designersReducer from "../containers/designers/reducer"

const rootReducer = combineReducers({
	app: appReducer,
	designer: designerReducer,
	designers: designersReducer
})

export default rootReducer