import React from 'react'
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import { Provider } from 'react-redux'
import PropTypes from 'prop-types'

import App from './App'
import ErrorBoundary from './components/error-boundary'
import Loading from './components/loading'

import Loadable from 'react-loadable'

const SPALoadable = loader => {
    const loadable = Loadable({
        loader,
        loading: Loading
    })

    return loadable
}

const spaErrorBoundary = (props) => (Component) => (
    <ErrorBoundary>
        <Component {...props} />
    </ErrorBoundary>
)

export const Home = SPALoadable(() =>
    import('./containers/home' /* webpackChunkName: "home" */)
)
export const Designer = SPALoadable(() =>
    import('./containers/designer' /* webpackChunkName: "designer" */)
)
export const Designers = SPALoadable(() =>
    import('./containers/designers' /* webpackChunkName: "designers" */)
)

const Routes = ({ store }) => (
  <Provider store={store}>
    <Router>
      <App>
        <Switch>
            <Route exact path="/" render={(props) => spaErrorBoundary(props)(Home)}/>
            <Route path="/designers/:id" render={(props) => spaErrorBoundary(props)(Designer)}/>
            <Route path="/designers" render={(props) => spaErrorBoundary(props)(Designers)} />
        </Switch>
      </App>
    </Router>
  </Provider>
)

Routes.propTypes = {
    store: PropTypes.object
}

export default Routes