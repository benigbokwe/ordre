export const mergePayload = (state, { payload }) => state.mergeDeep(payload)

/**
 * Makes an AJAX request to API endpoint
 * returns a json response
 * 
 * @param {string} url 
 * @param {object} options 
 */
export const makeGetRequestJson = (url, options = {}) => {
	//return a promise
	return fetch(url, options)
		.then((response) => response.json())
}
