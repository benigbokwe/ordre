import React from 'react'

import Header from './components/header'
import Footer from './components/footer'
import {Wrapper} from './components/styles/styles'

const App = (props) => (
  <Wrapper>
      <Header />
        {props.children}
      <Footer />
  </Wrapper>
)

export default App
