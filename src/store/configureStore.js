import { createStore, applyMiddleware, compose, combineReducers } from "redux"
import thunk from "redux-thunk"
import { createLogger } from "redux-logger"
import rootReducer from "../reducers"

// import DevTools from "../app/tools/DevTools"

const configureStore = preloadedState => {
	const reducer = combineReducers({
		ui: rootReducer,
	})

	const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

	const store = createStore(
		reducer,
		preloadedState,
		composeEnhancers(applyMiddleware(thunk, createLogger()))
	)

	return store
}

export default configureStore