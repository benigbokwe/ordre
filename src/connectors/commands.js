import {
	makeGetRequestJson
} from "../utils/utils"

import {
	receiveDesignerData,
	receiveDesignersData
} from "../app/create-actions"

export const initDesignersData = (url, options) => (dispatch) => {
	return makeGetRequestJson(url, options)
		.then((response) => {
			if (response.errors) {
				// Exception will be magically handled by Errorboundary 
				throw new Error('Something went wrong while fetching designers data!')
			}

			const {designers} = response
			dispatch(receiveDesignersData({
				designersList: designers,
				isDesignersLoaded: true
			}))
		})
}

export const initDesignerData = (url, options) => (dispatch) => {
	return makeGetRequestJson(url, options)
		.then((response) => {

			if (response.errors) {
				// Exception will be magically handled by Errorboundary 
				throw new Error('Something went wrong while fetching designer\'s data!')
			}

			const {designer} = response

			dispatch(receiveDesignerData({
				designerData: designer,
				isDesignerLoaded: true
			}))
		})
}