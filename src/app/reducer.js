import {handleActions} from 'redux-actions'
import Immutable from "immutable"
import { mergePayload } from "../utils/utils"

import {setPageFetchError} from '../app/create-actions'

export default handleActions(
    {
        [setPageFetchError]: mergePayload
    },

    Immutable.Map()
)
