import { createAction } from "redux-actions"

export const receiveDesignerData = createAction("RECEIVE_DESIGNER_DATA")
export const receiveDesignersData = createAction("RECEIVE_DESIGNERS_DATA")

export const setPageFetchError = createAction('SET_PAGE_FETCH_ERROR')