import {createSelector} from 'reselect'
import {createGetSelector} from 'reselect-immutable-helpers'
import { getUi } from "../utils/selector-utils"

export const getApp = createSelector(getUi, ({app}) => app)

export const getPageFetchError = createGetSelector(getApp, 'fetchError')